<?php

/**
 * PHP code as a Context condition.
 */
class contextphp_condition_php extends context_condition {

  /**
   * Omit condition values. We will provide a custom input form for our conditions.
   */
  function condition_values() {
    return array();
  }

  /**
   * Condition form.
   */
  function condition_form($context) {
    $form = parent::condition_form($context);
    unset($form['#options']);
    $form['#type'] = 'textarea';
    $form['#default_value'] = implode("\n", $this->fetch_from_context($context, 'values'));
    return $form;
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {
    return array($values);
  }

  /**
   * Execute.
   */
  function execute() {
    foreach (context_enabled_contexts() as $context) {
      $values = $this->fetch_from_context($context, 'values');
      if (!empty($values[0])) {
        $code = '<?php ' . $values[0] . ' ?>';
        $return = drupal_eval($code);
        if ($return == TRUE) {
          $this->condition_met($context, $return);
        }
      }
    }
  }
}
